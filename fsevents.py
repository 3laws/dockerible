import time
import argparse
import os
import subprocess
import sys
import traceback
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import PatternMatchingEventHandler

class TestEventHandler(PatternMatchingEventHandler):
    container_name = ""
    source_path_base = ""
    dest_path_base = ""
    base_dir = ""
    to_watch = []
    to_ignore = []
    rsync_watch = []
    rsync_ignore = []
    last_time = 0
    git_last_time = 0
    time_wait = 0

    def set_rsync(self):
        for item in self.to_ignore:
            self.rsync_ignore = self.rsync_ignore + ['--exclude', item]
        for item in self.to_watch:
            if item != '*':
                self.rsync_watch = self.rsync_watch + ['--include', item]

    def on_any_event(self, event):
        if (event.src_path.find(os.sep+'node_modules'+os.sep) == -1) and (not event.src_path.endswith(os.sep+'node_modules')):
            if event.event_type == 'deleted':
                mod_file_path = os.path.split(event.src_path)[0]+os.sep
            elif (event.is_directory):
                mod_file_path = event.src_path+os.sep
                print "directory only"
            else:
                mod_file_path = event.src_path
            relative_file_path = mod_file_path.split(self.base_dir+os.sep, 1)[1]
            repo_path = relative_file_path.split(os.sep, 1)[0]+os.sep
            if relative_file_path.find(os.sep+'.git'+os.sep) > -1:
                repo_path = relative_file_path.split(os.sep+'.git'+os.sep)[0]+os.sep+'.git'+os.sep
                #print "it's a git thing"
                time_since = time.time() - self.git_last_time
            else:
                time_since = time.time() - self.last_time

            #print("event noticed: " + event.event_type + " on file " + mod_file_path + " at " + time.asctime())

            if time_since > self.time_wait: #or relative_file_path.find(os.sep+'.git'+os.sep) > -1:

                if self.container_name == 'self':
                    source_path = os.path.join(self.source_path_base, relative_file_path)
                    dest_path = os.path.join(self.dest_path_base, relative_file_path)
                    try:
                        subprocess.check_output(['mkdir', '-p', os.path.split(dest_path)[0]])
                        subprocess.check_output(['rsync', '-avi', '--delete'] + self.rsync_watch + self.rsync_ignore + [source_path, dest_path])
                        print "\nsync complete from %s to %s" % (source_path, dest_path)
                    except subprocess.CalledProcessError as err:
                        print "\n***************docker rsync error: %s" % err
                else:
                    source_path = os.path.join(self.source_path_base, repo_path)
                    dest_path = os.path.join(self.dest_path_base, repo_path)
                    try:
                        subprocess.check_output(['docker', 'exec', '-t', self.container_name, 'mkdir', '-p', os.path.split(dest_path)[0]])
                        subprocess.check_output(['docker', 'exec', '-t', self.container_name, 'rsync', '-avi', '--delete'] + self.rsync_watch + self.rsync_ignore + [source_path, dest_path])
                        #print "docker exec -t %s rsync -avi --delete %s %s %s %s" % (self.container_name, self.rsync_watch, self.rsync_ignore, source_path, dest_path)
                        print "\nsync complete from %s to %s" % (source_path, dest_path)
                    except subprocess.CalledProcessError as err:
                        print "\n***************docker rsync error: %s" % err

                if relative_file_path.find(os.sep+'.git'+os.sep) == -1:
                    self.last_time = time.time()
                    print time.time(), self.last_time, time_since
                else:
                    self.git_last_time = time.time()
                    print time.time(), self.git_last_time, time_since


if __name__ == "__main__":
    argv=sys.argv[1:]  # pragma NO COVER
    print ("arguments: %s" % argv)
    parser = argparse.ArgumentParser("rsync into containers for fsevents.")
    parser.add_argument("-p", "--path", type=str, default="", help="path to repo directory to watch")
    parser.add_argument("-c", "--container", type=str, default="self", help="container name to run rsync in")
    parser.add_argument("-s", "--src", type=str, default="", help="src path inside the container")
    parser.add_argument("-d", "--dest", type=str, default="", help="dest path inside the container")
    parser.add_argument("-f", "--files_watch", type=str, default=["*"], nargs='+', help="file regex to watch")
    parser.add_argument("-i", "--files_ignore", type=str, default=["*.swp", "*.pyc", "*node_modules*"], nargs='+', help="file regex to watch")
    parser.add_argument("-t", "--time_wait", type=str, default=1, help="delay between events")
    args = parser.parse_args(argv)

    event_handler = TestEventHandler(ignore_patterns=args.files_ignore+['^*.[a-zA-Z0-9]{6}$'], patterns=args.files_watch, ignore_directories=True)
    event_handler.source_path_base = args.src
    event_handler.dest_path_base = args.dest
    event_handler.container_name = args.container
    event_handler.base_dir = args.path
    event_handler.to_watch = args.files_watch
    event_handler.to_ignore = args.files_ignore
    event_handler.time_wait = int(args.time_wait)
    event_handler.set_rsync()
    observer = Observer()
    observer.schedule(event_handler, path=args.path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
